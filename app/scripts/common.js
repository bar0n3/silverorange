$(function () {
	'use strict';
	// init jelect
	$('.jelect').jelect();
	// init custom scrollbar
	$('.js-custom-scrollbar').mCustomScrollbar();
	// select header menu items
	$('.js-menu').click(function () {
		$('.js-menu').removeClass('active');
		$(this).addClass('active');
	});
	// sticky header on scrolling
	$(document).scroll(function () {
		var $header_bp = 2, // header breakpoint pos
			$scrolltop = $(this).scrollTop();
		if ($scrolltop > $header_bp) {
			$('.b-header')
				.addClass('fixed')
				.removeClass('ontop');
		} else {
			$('.b-header')
				.addClass('ontop')
				.removeClass('fixed');
		}
	});
	// scalable font-size
	$(window).on('resize load', function () {
		var $fs,
			$ww = $(window).width();
		if ($ww < 1600 && $ww > 767) {
			$fs = parseInt(String($ww / 1600).substring(0, 4) * 100);
		} else if ($ww <= 767) {

		} else {
			$fs = '100';
		}
		document.body.style.fontSize = $fs + '%'; 
	});
	// preloader setup // timeouts are JFF
	$(document).ready(function () {
		setTimeout(function () {
			$('.js-preloader').removeClass('active');
		}, 8000);
		setTimeout(function () {
			$('.main').addClass('active');
		}, 8000);
		setTimeout(function () {
			$('.b-header').addClass('active');
		}, 8500);
	});
	// slider
	new Swiper('.swiper-container', {
		slidesPerView: 'auto',
		spaceBetween: 30,
		loop: true,
		autoplay: 500
	});
	// validate email
	function validateEmail($this) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
			email = $this.val();
		if (re.test(email)) {
			$this
				.addClass('valid')
				.removeClass('invalid');
		} else {
			$this
				.addClass('invalid')
				.removeClass('valid');
		}
	}
	$('.js-email').on({
		keypress: function () {
			validateEmail($(this));
		},
		blur: function () {
			$(this).removeClass('valid');
		}
	});
});
